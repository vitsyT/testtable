import React from 'react'
import {StyleSheet, View} from 'react-native'
import Table from '../../components/Table'
import Seat from '../../components/Seat'
import Dealer from '../../components/Dealer'
import constants from '../../constants'

const { numSeats, seatsXY, sxOfs, syOfs } = constants

export default class GameTable extends React.Component {
  state= { dealerPosition: 1 }

  componentWillMount() {
    this.timer = setInterval( () => {
      let dealerPosition  = this.state.dealerPosition
      if (++dealerPosition > numSeats) {
        dealerPosition = 1
      }
      this.setState({dealerPosition})
    }, 3000)
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer)
    }
  }

  render() {
    const { dealerPosition } = this.state
    const seats = constants.seatsXY.map((s, i) => {
      const stl = {
        position: 'absolute',
        left: s[0],
        top: s[1]
      }
      return (<Seat style={stl} key={i}></Seat>)
    })

    const repers = seatsXY.map((s, i) => {
      const stl = {
        position: 'absolute',
        width: 4,
        height: 4,
        backgroundColor: '#F80', left: s[0] + sxOfs - 2, top: s[1] + syOfs - 2
      }
      return (<View style={stl} key={i}></View>)
    })

    return (
       <View style={styles.container}>
         <Table>
           {seats}
           {repers}
           <Dealer position={dealerPosition} />
         </Table>
       </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    padding: 0,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#AAA'
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  table: {
    width: 700,
    height: 510
  },
  desk: {},
  img: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1
  }
})
