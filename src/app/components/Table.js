import React from 'react'
import {Dimensions, StyleSheet,  View, Image} from 'react-native'
import tableImage from '../assets/table.png'
import constants from '../constants'

export const BKG_WIDTH = 700
export const BKG_HEIGHT = 510
const BKG_RATIO  = BKG_HEIGHT / BKG_WIDTH

export default class Deck extends React.Component {
  state = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  }


  componentWillMount() {
    Dimensions.addEventListener('change', (e) => {
      const {width, height} = e.window
      this.setState({width, height})
    })
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this._handleChange)
  }

  render() {
    var {width, height } = this.state
    let scale = width / BKG_WIDTH
    if (width * BKG_RATIO > height) {
      scale = height / BKG_HEIGHT
    }

    return (
     <View  style={[styles.container, { transform:[{scale}]}] }>
        <Image
            resizeMode={Image.resizeMode.contain}
            source={tableImage}
            style={[styles.tableImg, { height: BKG_HEIGHT, width: BKG_WIDTH}]}>
        </Image>
        {this.props.children}
    </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: BKG_HEIGHT,
    width: BKG_WIDTH,
    position:'absolute',
  },


  bkgImg: {
    position: 'absolute',
    left:0,
    top:0,
    bottom: 0,
    right: 0,
    zIndex: -1
  }
})

