import React from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Easing,
  Image,
  Animated } from 'react-native'
import dealerImage from '../assets/dealer.png'
import constants from '../constants'
/*
 <div class="dealer" style="left: 516px; top: 137px; background: url(&quot;Image?Name=Chips&amp;Crc=8556B0E0&quot;) 0px 0px no-repeat; display: block; opacity: 1; transition: left 500ms ease-out, top 500ms ease-out; backface-visibility: hidden;"></div>
 g = b8.seatPosition(1);
 b7 = b8.dealerX[b2][g] - b8.dxOfs;
 b6 = b8.dealerY[b2][g] - b8.dyOfs;
 b8.$dealer = $("<div>").addClass("dealer").css({
 left: b7,
 top: b6,
 background: "url('Image?Name=Chips&Crc=" + bd.crc.image + "') no-repeat 0px 0px"
 }).appendTo(b8.$content);
 b8.graphicsMade = true
 */
const { dealerX, dealerY, dxOfs, dyOfs, numSeats } = constants

export default class Dealer extends React.Component {
  animated = new Animated.Value(0);
  state = {}

  componentWillReceiveProps(nextProps) {
    this.animated.addListener(({value}) => {
      this._animatedValue = value
    })
    const { position } = this.props
    if (nextProps.position !== position ) {
      this.animate(nextProps);
    }
  }

  animate({ position }) {
   const oldX = dealerX[numSeats][this.props.position] - dxOfs
   const oldY = dealerY[numSeats][this.props.position] - dyOfs
   const newX = dealerX[numSeats][position] - dxOfs
   const newY = dealerY[numSeats][position] - dyOfs
   this.animated.setValue(0)
   this.animating  = true
   this.setState({
      translateX: this.animated.interpolate({inputRange: [0, 1], outputRange: [oldX, newX]}),
      translateY: this.animated.interpolate({inputRange: [0, 1], outputRange: [oldY, newY]})
    });
    Animated.timing(this.animated, {
      toValue: 1,
      easing: Easing.out(Easing.quad),
      duration: 1000,
    }).start(() => {
      this.animating  = false
      this.oldX = newX
      this.oldY = newY
    });
  }

  render() {
    let style
    const { position } = this.props
    const { translateX, translateY } = this.state;

    if ( this.animating) {
      style = {
        left:0,
        top:0,
        transform: [{ translateX }, { translateY }]
      }
    } else {
      this.oldX = dealerX[numSeats][position] - dxOfs
      this.oldY = dealerY[numSeats][position] - dyOfs
      style = {
        left: this.oldX,
        top: this.oldY
      }
    }
    return (
       <Animated.Image
            source={dealerImage}
            style={[styles.dealer, style]}/>
    )
  }
}

Dealer.propTypes = {
  position: PropTypes.number.isRequired
};

Dealer.defaultProps = {
  position: 1
};
const styles = StyleSheet.create({
  dealer: {
    width: 23,
    height: 19,
    position: 'absolute'
  }
})


