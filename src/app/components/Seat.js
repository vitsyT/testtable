import React from 'react'
import {StyleSheet,  View, Image} from 'react-native'
import seatImage from '../assets/seatEmpty.png'

/* Seat holder */
/*<div class="hide" style="width: 130px; height: 76px; top: 154px; left: 562px; display: block;">
 <div class="sp_cards" style="top: 0px; left: 0px; right: 0px; height: 38px;"></div>
 <div class="sp_graphic" style="top: 38px; left: 0px; right: 0px; bottom: 0px; background: url(&quot;Image?Name=SeatEmpty&amp;Crc=8556B0E0&quot;) no-repeat; opacity: 0.15;"></div>
 <div class="sp_seat" style="top: 38px; left: 0px; right: 0px; bottom: 0px;">
 <div class="sp_avatar" style="top: 3px; left: 3px; width: 32px; height: 32px; background-image: none;">
 <div class="sp_block" style="top: 0px; right: 0px; width: 10px; height: 12px; background-image: none;"></div>
 <div class="sp_note" style="bottom: 0px; left: 0px; width: 8px; height: 8px; right: auto; display: none; background-color: rgb(255, 255, 255);"></div>
 </div>
 <div class="sp_name center" style="top: 2px; left: 38px; width: 90px; height: 16px;"></div>
 <div class="sp_info center" style="top: 20px; left: 38px; width: 90px; height: 16px;"></div>
 <div class="sp_glow" style="top: 0px; left: 0px; right: 0px; bottom: 0px; transition: opacity 0ms; backface-visibility: hidden; opacity: 0;"></div>
 </div>
 </div>
 */

export default class Seat extends React.Component {

  render() {
    return (
       <View style={[styles.container, this.props.style]} >
         <View style={styles.sp_cards}></View>
         <Image
            source={seatImage}
            style={[styles.sp_graphic]}/>
       </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: 130,
    height: 76,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    position: 'relative',
    borderColor: '#0F0',
    borderWidth:1,
    borderStyle:'dotted'
  },

  sp_cards: {
    height: 38,
    width: 130,
    backgroundColor:'transparent'
  },

  sp_graphic: {
    opacity: 0.15,
    width: 130,
    height: 38,
  }

})


