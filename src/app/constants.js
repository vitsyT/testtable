
function initConstants() {
  this.boardX = [0, 252, 301, 350, 399, 448];
  this.boardY = 210;
  this.chipX = [];
  this.chipX[2] = [0, 544, 156];
  this.chipX[3] = [0, 527, 350, 173];
  this.chipX[4] = [0, 527, 527, 173, 173];
  this.chipX[5] = [0, 527, 527, 350, 173, 173];
  this.chipX[6] = [0, 422, 544, 422, 278, 156, 278];
  this.chipX[7] = [0, 527, 544, 527, 350, 173, 156, 173];
  this.chipX[8] = [0, 422, 527, 527, 422, 278, 173, 173, 278];
  this.chipX[9] = [0, 422, 527, 544, 527, 350, 173, 156, 173, 278];
  this.chipX[10] = [0, 422, 527, 544, 527, 422, 278, 173, 156, 173, 278];
  this.chipY = [];
  this.chipY[2] = [0, 200, 200];
  this.chipY[3] = [0, 125, 285, 125];
  this.chipY[4] = [0, 125, 264, 264, 125];
  this.chipY[5] = [0, 125, 264, 285, 264, 125];
  this.chipY[6] = [0, 112, 200, 285, 285, 200, 112];
  this.chipY[7] = [0, 125, 200, 264, 285, 264, 200, 125];
  this.chipY[8] = [0, 112, 125, 264, 285, 285, 264, 125, 112];
  this.chipY[9] = [0, 112, 125, 200, 264, 285, 264, 200, 125, 112];
  this.chipY[10] = [0, 112, 125, 200, 264, 285, 285, 264, 200, 125, 112];
  this.chxOfs = 11;
  this.chyOfs = 9;
  this.cxOfs = 23;
  this.cyOfs = 32;
   this.dealerX = [];
   this.dealerX[2] = [0, 544, 156];
   this.dealerX[3] = [0, 527, 271, 173];
   this.dealerX[4] = [0, 527, 527, 173, 173];
   this.dealerX[5] = [0, 527, 527, 271, 173, 173];
   this.dealerX[6] = [0, 386, 544, 386, 314, 156, 314];
   this.dealerX[7] = [0, 527, 544, 527, 271, 173, 156, 173];
   this.dealerX[8] = [0, 386, 527, 527, 386, 314, 173, 173, 314];
   this.dealerX[9] = [0, 386, 527, 544, 527, 271, 173, 156, 173, 314];
   this.dealerX[10] = [0, 386, 527, 544, 527, 386, 314, 173, 156, 173, 314];
   this.dealerY = [];
   this.dealerY[2] = [0, 221, 221];
   this.dealerY[3] = [0, 146, 323, 146];
   this.dealerY[4] = [0, 146, 285, 285, 146];
   this.dealerY[5] = [0, 146, 285, 323, 285, 146];
   this.dealerY[6] = [0, 83, 221, 332, 332, 221, 83];
   this.dealerY[7] = [0, 146, 221, 285, 323, 285, 221, 146];
   this.dealerY[8] = [0, 83, 146, 285, 332, 332, 285, 146, 83];
   this.dealerY[9] = [0, 83, 146, 221, 285, 323, 285, 221, 146, 83];
   this.dealerY[10] = [0, 83, 146, 221, 285, 333, 333, 285, 221, 146, 83];
  this.dxOfs = 11;
  this.dyOfs = 9;
  //if (this.props.game === "holdem") {
    this.holeCards = 2;
    this.holeX = [0, 30, 16];
    this.holeY = [0, 40, 40]
  /*} else {
    if (this.props.game === "omaha") {
      this.holeCards = 4;
      this.holeX = [0, 44, 30, 16, 2];
      this.holeY = [0, 40, 40, 40, 40]
    } else {
      this.holeCards = 7;
      this.holeX = [0, 65, 51, 37, 23, 9, -5, -19];
      this.holeY = [0, 40, 40, 45, 45, 45, 45, 40]
    }
  }*/
  this.seatX = [];
  this.seatX[2] = [0, 627, 72];
  this.seatX[3] = [0, 608, 350, 92];
  this.seatX[4] = [0, 608, 608, 92, 92];
  this.seatX[5] = [0, 608, 608, 350, 92, 92];
  this.seatX[6] = [0, 468, 627, 468, 231, 72, 231];
  this.seatX[7] = [0, 608, 627, 608, 350, 92, 72, 92];
  this.seatX[8] = [0, 468, 608, 608, 468, 231, 92, 92, 231];
  this.seatX[9] = [0, 468, 608, 627, 608, 350, 92, 72, 92, 231];
  this.seatX[10] = [0, 468, 608, 627, 608, 468, 231, 92, 72, 92, 231];
  this.seatY = [];
  this.seatY[2] = [0, 192, 192];
  this.seatY[3] = [0, 102, 341, 102];
  this.seatY[4] = [0, 102, 282, 282, 102];
  this.seatY[5] = [0, 102, 282, 341, 282, 102];
  this.seatY[6] = [0, 52, 192, 341, 341, 192, 52];
  this.seatY[7] = [0, 102, 192, 282, 341, 282, 192, 102];
  this.seatY[8] = [0, 52, 102, 282, 341, 341, 282, 102, 52];
  this.seatY[9] = [0, 52, 102, 192, 282, 341, 282, 192, 102, 52];
  this.seatY[10] = [0, 52, 102, 192, 282, 341, 341, 282, 192, 102, 52];
  this.sxOfs = 65;
  this.syOfs = 38;
  this.potX = [0, 350, 251, 448, 251, 448, 316, 384, 302, 398];
  this.potY = [0, 153, 153, 153, 255, 255, 255, 255, 153, 153]


  this.numSeats = 10;
  this.seatsXY = []
  for (let b4 = 1; b4 <= this.numSeats; b4++) {
    const g = b4//b8.seatPosition(b4);
    const b7 = this.seatX[this.numSeats][g];
    const b6 = this.seatY[this.numSeats][g];
    /*  b1 = (b8.game == "stud" || b8.game == "razz");
     for (b3 = 1; b3 <= 7; b3++) {
     if (b1 && b3 > 2 && b3 < 7) {
     b5 = 45
     } else {
     b5 = 40
     }
     b8.card[b3][b4] = new bQ(b8,b5,b7 - b8.holeX[b3],b6 - b8.holeY[b3]);
     b8.card[b3][b4].clip(true)
     }*/
    this.seatsXY.push([b7 - this.sxOfs, b6 - this.syOfs])

  }
}

const constants = new initConstants()
export default constants
