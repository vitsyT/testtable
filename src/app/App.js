import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import GameTable from './containers/GameTable'

class App extends Component {
  render() {
    return (
      <View style={styles.container} >
        <GameTable/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#dfd',
    minHeight: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },

})

export default App;
